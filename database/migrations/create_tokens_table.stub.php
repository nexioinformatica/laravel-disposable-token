<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTokensTable extends Migration
{
    public function up()
    {
        Schema::create(config('tokens_table_name', 'tokens'), function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('token');
            $table->timestamp('activate_at');
            $table->timestamp('deactivate_at')->nullable();

            $table->timestamps();
        });
    }
}
