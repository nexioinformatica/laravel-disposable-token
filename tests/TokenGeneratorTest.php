<?php

declare(strict_types=1);

namespace Nexio\DisposableToken;

use InvalidArgumentException;
use Nexio\DisposableToken\Token\Generator\TokenGeneratorFactory;
use PHPUnit\Framework\TestCase;

class TokenGeneratorTest extends TestCase
{
    public function testItGeneratesStringToken()
    {
        $this->assertTrue(is_string(TokenGeneratorFactory::random()->token()));
        $this->assertTrue(is_string(TokenGeneratorFactory::fixed('foo')->token()));
    }

    public function testItGeneratesRandomToken()
    {
        $token1 = TokenGeneratorFactory::random()->token();
        $token2 = TokenGeneratorFactory::random()->token();
        $this->assertNotEquals($token1, $token2);
    }

    public function testItGeneratesTheGivenFixedToken()
    {
        $token1 = TokenGeneratorFactory::fixed('foo')->token();
        $token2 = TokenGeneratorFactory::fixed('foo')->token();
        $token3 = TokenGeneratorFactory::fixed('bar')->token();
        $this->assertEquals($token1, $token2);
        $this->assertNotEquals($token1, $token3);
    }

    public function testItGeneratesTokenWithRightLength()
    {
        $this->assertEquals(8, strlen(TokenGeneratorFactory::random()->token()));
        $this->assertGreaterThan(0, strlen(TokenGeneratorFactory::fixed('foo')->token()));
    }

    public function testItGeneratesExceptionIfFixedTokenIsEmpty()
    {
        $this->expectException(InvalidArgumentException::class);
        TokenGeneratorFactory::fixed('')->token();
    }
}
