<?php

declare(strict_types=1);

namespace Nexio\DisposableToken;

use Carbon\Carbon;
use Carbon\CarbonInterval;
use Nexio\DisposableToken\Token\Model\TokenFactory;
use PHPUnit\Framework\TestCase;

class TokenTest extends TestCase
{
    public function testItChecksValidityBasedOnTimeToLive()
    {
        $this->assertTrue(TokenFactory::makeDefault()->isValid());
        $this->assertTrue(TokenFactory::makeWithTtl(CarbonInterval::days(7))->isValid());
        $this->assertTrue(TokenFactory::makeEndlessToken()->isValid());
        $this->assertFalse(TokenFactory::makeDead()->isValid());
    }

    public function testItChecksValidityBasedOnActivationDate()
    {
        $this->assertTrue(TokenFactory::makeWithActivation(Carbon::now())->isValid());
        $this->assertTrue(TokenFactory::makeWithActivation(Carbon::now()->subDay())->isValid());
        $this->assertFalse(TokenFactory::makeWithActivation(Carbon::now()->addDay())->isValid());
        $this->assertFalse(TokenFactory::makeEndlessTokenWithActivation(Carbon::now()->addDay())->isValid());
    }

    public function testItChecksValidityBasedOnActivationDateAndTimeToLive()
    {
        $this->assertTrue(
            TokenFactory::make(Carbon::now(), CarbonInterval::days(1))->isValid()
        );
        $this->assertFalse(
            TokenFactory::make(Carbon::now()->addDay(), CarbonInterval::days(1))->isValid()
        );
    }

    public function testItRevokesTheToken()
    {
        $this->assertFalse(TokenFactory::makeDefault()->dead()->isValid());
        $this->assertFalse(TokenFactory::makeEndlessToken()->dead()->isValid());
    }

    public function testItCanExtendTokenLife()
    {
        $this->assertTrue(TokenFactory::makeDead()->extend(CarbonInterval::days(1))->isValid());
        $this->assertTrue(
            TokenFactory::makeWithActivation(Carbon::now()->subDay())->extend(CarbonInterval::days(2))->isValid()
        );
        $this->assertFalse(TokenFactory::makeDead()->extend(CarbonInterval::days(0))->isValid());
        $this->assertFalse(
            TokenFactory::makeWithActivation(Carbon::now()->addDay())->extend(CarbonInterval::days(1))->isValid()
        );
        $this->assertTrue(TokenFactory::makeEndlessToken()->extend(CarbonInterval::days(1))->isValid());
        $this->assertFalse(TokenFactory::makeEndlessToken()->extend(CarbonInterval::seconds(0))->isValid());
    }

    public function testItChecksWhetherTokenIsExpiredOrNot()
    {
        $this->assertFalse(TokenFactory::makeWithActivation(Carbon::now()->addDay())->isExpired());
        $this->assertFalse(TokenFactory::makeEndlessTokenWithActivation(Carbon::now()->addDay())->isExpired());
        $this->assertFalse(TokenFactory::makeWithTtl(CarbonInterval::day())->isExpired());
        $this->assertTrue(TokenFactory::makeDead()->isExpired());
        $this->assertTrue(TokenFactory::make(Carbon::now()->subDay(), CarbonInterval::hour())->isExpired());
    }
}
