<?php


namespace Nexio\DisposableToken;

use Carbon\Carbon;
use Carbon\CarbonInterval;
use Nexio\DisposableToken\Token\Model\TokenFactory;
use PHPUnit\Framework\TestCase;

class TokenFactoryTest extends TestCase
{
    public function testItCreatesTokenWithActivateAtAndTimeToLive()
    {
        $now = Carbon::now();
        $ttl = CarbonInterval::days(3);
        $expire_at = $now->clone()->add($ttl);

        $token = TokenFactory::make($now, $ttl);

        $this->assertEquals($now, $token->activate_at);
        $this->assertEquals($expire_at, $token->deactivate_at);
    }
}
