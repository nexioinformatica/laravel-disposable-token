<?php

declare(strict_types=1);

namespace Nexio\DisposableToken;

use Carbon\Carbon;
use Carbon\CarbonInterval;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;

/**
 * @property Carbon $activate_at
 * @property Carbon $deactivate_at
 * @property string $token
 * @property string $description
 *
 * @mixin EloquentBuilder
 */
class Token extends Model
{
    protected $table = 'tokens';
    protected $fillable = ['token', 'activate_at', 'deactivate_at', 'created_at', 'updated_at'];

    /**
     * @param array $attributes
     * @return Token A new token with attributes `$attributes`, not related to eloquent
     */
    public static function make(array $attributes = array()): Token
    {
        $x = new Token();
        foreach ($attributes as $k => $v) {
            $x->{$k} = $v;
        }
        return $x;
    }

    /**
     * @return $this Current dead token
     */
    public function dead(): self
    {
        $this->deactivate_at = $this->activate_at->clone();
        return $this;
    }

    /**
     * @param $interval CarbonInterval|null The interval to add to activation date for expiry date.
     *                                      If null token is endless.
     * @return $this Current token with new expire interval
     */
    public function extend(?CarbonInterval $interval): self
    {
        if (is_null($interval)) {
            $this->deactivate_at = null;
            return $this;
        }

        $this->deactivate_at = $this->activate_at->clone()->add($interval);

        return $this;
    }

    /** @return bool Whether the token is valid or not */
    public function isValid() : bool
    {
        $now = Carbon::now();

        if (is_null($this->deactivate_at)) {
            return $now->isAfter($this->activate_at);
        }

        return $now->isBetween($this->activate_at, $this->deactivate_at);
    }

    /**
     * A token is expired if we are after it's deactivation date.
     * @return bool Whether the token is expired or not
     */
    public function isExpired(): bool
    {
        $now = Carbon::now();

        if (is_null($this->deactivate_at)) {
            return false;
        }

        return $now->isAfter($this->deactivate_at);
    }

    public function getActivateAtAttribute($value)
    {
        if ($value instanceof Carbon) {
            return $value;
        }
        return Carbon::parse($value);
    }

    public function getDeactivateAtAttribute($value)
    {
        if (is_null($value)) {
            return $value;
        }
        if ($value instanceof Carbon) {
            return $value;
        }
        return Carbon::parse($value);
    }
}
