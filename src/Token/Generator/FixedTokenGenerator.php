<?php


namespace Nexio\DisposableToken\Token\Generator;

class FixedTokenGenerator extends TokenGenerator
{
    protected $customToken;

    public function __construct(string $token)
    {
        if ($token == '') {
            throw new \InvalidArgumentException('Fixed token cannot be empty string.');
        }
        $this->customToken = $token;
    }

    public function token(): string
    {
        return $this->customToken;
    }
}
