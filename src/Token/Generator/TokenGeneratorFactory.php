<?php


namespace Nexio\DisposableToken\Token\Generator;

class TokenGeneratorFactory
{
    public static function random() : TokenGenerator
    {
        return new RandomBytesTokenGenerator();
    }

    public static function fixed($token): TokenGenerator
    {
        return new FixedTokenGenerator($token);
    }
}
