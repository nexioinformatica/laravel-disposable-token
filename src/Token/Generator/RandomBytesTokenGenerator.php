<?php


namespace Nexio\DisposableToken\Token\Generator;

class RandomBytesTokenGenerator extends TokenGenerator
{
    public function token(): string
    {
        return bin2hex(random_bytes(4));
    }
}
