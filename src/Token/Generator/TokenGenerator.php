<?php


namespace Nexio\DisposableToken\Token\Generator;

abstract class TokenGenerator
{
    abstract public function token(): string;
}
