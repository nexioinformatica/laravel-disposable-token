<?php


namespace Nexio\DisposableToken\Token\Model;

use Carbon\Carbon;
use Carbon\CarbonInterval;
use Nexio\DisposableToken\Token;
use Nexio\DisposableToken\Token\Generator\TokenGeneratorFactory;

class TokenFactory
{
    public static function make(Carbon $activate_at, ?CarbonInterval $time_to_live)
    {
        return Token::make([
            'token' => TokenGeneratorFactory::random()->token(),
            'activate_at' => $activate_at,
            'deactivate_at' => !is_null($time_to_live) ? $activate_at->clone()->add($time_to_live) : null ]);
    }

    /**
     * Create a token valid from now up to 3 days.
     * @return Token
     */
    public static function makeDefault()
    {
        return self::make(Carbon::now(), CarbonInterval::days(3));
    }

    public static function makeDead()
    {
        return self::make(Carbon::now(), CarbonInterval::seconds(0));
    }

    public static function makeWithActivation(Carbon $activate_at)
    {
        return self::make($activate_at, CarbonInterval::days(3));
    }

    public static function makeWithTtl(CarbonInterval $ttl)
    {
        return self::make(Carbon::now(), $ttl);
    }

    public static function makeEndlessTokenWithActivation(Carbon $activate_at)
    {
        return self::make($activate_at, null);
    }

    public static function makeEndlessToken()
    {
        return self::makeEndlessTokenWithActivation(Carbon::now());
    }
}
