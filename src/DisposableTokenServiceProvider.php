<?php


namespace Nexio\DisposableToken;

use Illuminate\Support\ServiceProvider;

/**
 * @codeCoverageIgnore
 */
class DisposableTokenServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->registerPublishables();
    }

    public function register()
    {
    }

    protected function registerPublishables()
    {
        if (! class_exists('CreateTokensTable')) {
            $this->publishes([
                __DIR__.'/../database/migrations/create_tokens_table.stub.php'
                    => database_path('migrations/'.date('Y_m_d_His', time()).'_create_tokens_table.php'),
            ], 'migrations');
        }
    }
}
